/*
* Author: Woobin Im
* Description: VGG-face feature extractor.
*/
#include "vggface.h"
#include "wb_relu.hpp"
#include <data.hpp>
#include <cuda.h>

namespace vggface {
// Network parameter memories
std::string vggpath = "";
std::vector<std::string> w_list, b_list;
std::vector<vl::TensorShape> w_tlist, b_tlist, ts_list;
std::vector<vl::Tensor> w_dlist, b_dlist;

// copy values in binary file to memory.
void copyFromBinary(std::string filename, void *data, size_t size){
	std::ifstream fin;
	fin.open(filename, std::ios::binary | std::ios::in);
	fin.read((char*)data, size);
	fin.close();
}

vl::TensorShape tsWithBatch(vl::TensorShape shape, int batch){
	shape.setSize(batch);
	return shape;
}

// delete tensor memory.
void deleteTensor(vl::Tensor tensor){
	if(tensor.getDeviceType() == vl::Device::CPU)
		delete [] tensor.getMemory();
	else
		cudaFree(tensor.getMemory()) ;
}

// move values in gpu tensor to cpu memory
void* moveCPU(vl::Tensor tensor){
	if(tensor.getDeviceType() == vl::Device::GPU){
		int size = tensor.getShape().getNumElements()*sizeof(tensor.getDataType());
		void* ongpu = tensor.getMemory();
		void* oncpu = new char[size];
		cudaMemcpy(oncpu, ongpu, size, cudaMemcpyDeviceToHost);
		deleteTensor(tensor);
		return oncpu;
	}
	else
		return tensor.getMemory();
}

// make float gpu tensor from float array
vl::Tensor makeTensor(vl::TensorShape shape, void *data, size_t size, vl::Device device){
	vl::Tensor temp(shape,vl::vlTypeFloat, device, data, size);
	return temp;
}

// make float gpu tensor from file
vl::Tensor makeTensorFromFile(std::string filename, vl::TensorShape shape, void *data, size_t size, vl::Device device){
	void *buff = new char[size];
	copyFromBinary(filename, buff, size);
	if(device == vl::Device::GPU){
		cudaMemcpy(data, buff, size, cudaMemcpyHostToDevice); 
	}
	else
		data = memcpy(data, buff, size);
	delete [] buff;
	vl::Tensor temp(shape,vl::vlTypeFloat, device, data, size);
	return temp;
}

// vgg convolution
vl::Error vgg_conv(vl::Context ctx, vl::Tensor x, vl::Tensor w, vl::Tensor b, vl::Tensor y){
	return nnconv_forward(ctx, y, 1, x, 1, w, b, 1, 1, 1, 1, 1, 1);
}

// vgg fully connected
vl::Error vgg_fc(vl::Context ctx, vl::Tensor x, vl::Tensor w, vl::Tensor b, vl::Tensor y){
	return nnfullyconnected_forward(ctx, y, x, w, b) ;
}

// vgg pooling
vl::Error vgg_pool(vl::Context ctx, vl::Tensor x, vl::Tensor y){
	return nnpooling_forward(ctx, y, x, vl::vlPoolingMax, 2, 2, 2, 2, 0, 0, 0, 0) ;
}

// vgg relu
void vgg_relu(vl::Context ctx, vl::Tensor x, vl::Tensor y){
	// this function is not part of MatConvNet
	wb_relu_forward(ctx, x, y);
}

// vgg convolution wrapping for the convinient use.
vl::Tensor vgg_conv_wrap(vl::Context ctx, vl::Tensor& iTensor, vl::Tensor& oTensor, wNum){
	vl::TensorShape shape = tsWithBatch(ts_list[wNum], iTensor.getSize());
	int size = shape.getNumElements()*sizeof(float);
	//void* conv;
	//cudaMalloc((void **)&conv, size);
	//vl::Tensor oTensor;
	//oTensor = makeTensor(shape,conv,size,vl::Device::GPU);
	vgg_conv(ctx, iTensor, w_dlist[wNum], b_dlist[wNum], oTensor);
	deleteTensor(iTensor);
	vgg_relu(ctx, oTensor, oTensor);
	return oTensor;
}

// vgg fully connected wrapping for the convinient use.
vl::Tensor vgg_fc_wrap(vl::Context ctx, vl::Tensor& iTensor, vl::Tensor& oTensor, int wNum, bool relu){
	vl::TensorShape shape = tsWithBatch(ts_list[wNum], iTensor.getSize());
	int size = shape.getNumElements()*sizeof(float);
	//void* conv;
	//cudaMalloc((void **)&conv, size);
	//vl::Tensor oTensor;
	//oTensor = makeTensor(shape,conv,size,vl::Device::GPU);
	vgg_fc(ctx, iTensor, w_dlist[wNum], b_dlist[wNum], oTensor);
	deleteTensor(iTensor);
	if (relu)
		vgg_relu(ctx, oTensor, oTensor);
	return oTensor;
}

// vgg pooling wrapping for the convinient use.
vl::Tensor vgg_pool_wrap(vl::Context ctx, vl::Tensor& iTensor, vl::Tensor& oTensor, vl::TensorShape shape){
	int size =  shape.getNumElements()*sizeof(float);
	//void* pool;
	//cudaMalloc((void **)&pool, size);
	//vl::Tensor oTensor;
	//oTensor = makeTensor(shape,pool,size, vl::Device::GPU);
	vgg_pool(ctx, iTensor, oTensor);
	deleteTensor(iTensor);
	return oTensor;
}

// VGG-FACE meta data input
void fillList(){

	ts_list.push_back(vl::TensorShape(224,224,64,1));
	ts_list.push_back(vl::TensorShape(224,224,64,1));
	w_list.push_back("conv1_1_w.dat");	
	b_list.push_back("conv1_1_b.dat");
	w_list.push_back("conv1_2_w.dat");
	b_list.push_back("conv1_2_b.dat");
	w_tlist.push_back(vl::TensorShape(3,3,3,64));
	b_tlist.push_back(vl::TensorShape(64,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,64,64));
	b_tlist.push_back(vl::TensorShape(64,1,1,1));

	ts_list.push_back(vl::TensorShape(112,112,128,1));
	ts_list.push_back(vl::TensorShape(112,112,128,1));
	w_list.push_back("conv2_1_w.dat");
	b_list.push_back("conv2_1_b.dat");
	w_list.push_back("conv2_2_w.dat");
	b_list.push_back("conv2_2_b.dat");
	w_tlist.push_back(vl::TensorShape(3,3,64,128));
	b_tlist.push_back(vl::TensorShape(128,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,128,128));
	b_tlist.push_back(vl::TensorShape(128,1,1,1));

	ts_list.push_back(vl::TensorShape(56,56,256,1));
	ts_list.push_back(vl::TensorShape(56,56,256,1));
	ts_list.push_back(vl::TensorShape(56,56,256,1));
	w_list.push_back("conv3_1_w.dat");
	b_list.push_back("conv3_1_b.dat");
	w_list.push_back("conv3_2_w.dat");
	b_list.push_back("conv3_2_b.dat");
	w_list.push_back("conv3_3_w.dat");
	b_list.push_back("conv3_3_b.dat");
	w_tlist.push_back(vl::TensorShape(3,3,128,256));
	b_tlist.push_back(vl::TensorShape(256,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,256,256));
	b_tlist.push_back(vl::TensorShape(256,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,256,256));
	b_tlist.push_back(vl::TensorShape(256,1,1,1));

	ts_list.push_back(vl::TensorShape(28,28,512,1));
	ts_list.push_back(vl::TensorShape(28,28,512,1));
	ts_list.push_back(vl::TensorShape(28,28,512,1));
	w_list.push_back("conv4_1_w.dat");
	b_list.push_back("conv4_1_b.dat");
	w_list.push_back("conv4_2_w.dat");
	b_list.push_back("conv4_2_b.dat");
	w_list.push_back("conv4_3_w.dat");
	b_list.push_back("conv4_3_b.dat");
	w_tlist.push_back(vl::TensorShape(3,3,256,512));
	b_tlist.push_back(vl::TensorShape(512,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,512,512));
	b_tlist.push_back(vl::TensorShape(512,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,512,512));
	b_tlist.push_back(vl::TensorShape(512,1,1,1));

	ts_list.push_back(vl::TensorShape(14,14,512,1));
	ts_list.push_back(vl::TensorShape(14,14,512,1));
	ts_list.push_back(vl::TensorShape(14,14,512,1));
	w_list.push_back("conv5_1_w.dat");
	b_list.push_back("conv5_1_b.dat");
	w_list.push_back("conv5_2_w.dat");
	b_list.push_back("conv5_2_b.dat");
	w_list.push_back("conv5_3_w.dat");
	b_list.push_back("conv5_3_b.dat");
	w_tlist.push_back(vl::TensorShape(3,3,512,512));
	b_tlist.push_back(vl::TensorShape(512,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,512,512));
	b_tlist.push_back(vl::TensorShape(512,1,1,1));
	w_tlist.push_back(vl::TensorShape(3,3,512,512));
	b_tlist.push_back(vl::TensorShape(512,1,1,1));

	ts_list.push_back(vl::TensorShape(1,1,4096,1));
	ts_list.push_back(vl::TensorShape(1,1,4096,1));
	ts_list.push_back(vl::TensorShape(1,1,2622,1));
	w_list.push_back("fc6_w.dat");
	b_list.push_back("fc6_b.dat");
	w_list.push_back("fc7_w.dat");
	b_list.push_back("fc7_b.dat");
	w_list.push_back("fc8_w.dat");
	b_list.push_back("fc8_b.dat");
	w_tlist.push_back(vl::TensorShape(7,7,512,4096));
	b_tlist.push_back(vl::TensorShape(4096,1,1,1));
	w_tlist.push_back(vl::TensorShape(1,1,4096,4096));
	b_tlist.push_back(vl::TensorShape(4096,1,1,1));
	w_tlist.push_back(vl::TensorShape(1,1,4096,2622));
	b_tlist.push_back(vl::TensorShape(2622,1,1,1));
}

// FUNCTION: initializeNet
// PURPOSE: initialize the network.
// CAUTION: only use it once when start.
void initializeNet(std::string netpath){
	vggpath = netpath;
	w_list.clear();
	b_list.clear();
	w_tlist.clear();
	b_tlist.clear();
	ts_list.clear();
	w_dlist.clear();
	b_dlist.clear();
	fillList();
	for(int i=0; i< w_list.size(); i++){
		int weight_size = w_tlist[i].getNumElements();
		void* w;
		cudaMalloc((void **)&w, weight_size*sizeof(float));
		vl::Tensor w_tensor = makeTensorFromFile(vggpath + w_list[i],w_tlist[i],w,weight_size*sizeof(float),vl::Device::GPU);

		int bias_size = b_tlist[i].getNumElements();
		void* b;
		cudaMalloc((void **)&b, bias_size*sizeof(float));
		vl::Tensor b_tensor = makeTensorFromFile(vggpath + b_list[i],b_tlist[i],b,bias_size*sizeof(float),vl::Device::GPU);

		w_dlist.push_back(w_tensor);
		b_dlist.push_back(b_tensor);
	}
}

// FUNCTION: vgg_process
// PURPOSE: feed forward an 224x224x3 float image to get the feature
float* vgg_process(float *input, int batch){
	vl::Context ctx;
	vl::Tensor iTensor, tensor;
	int imgSize = 224*224*3;
	int inputSize = imgSize*batch;

	//Input
	float * input_copy = new float[inputSize];
	
	int g_cnt = 0;
	int channelSize = imgSize/3;
	float imgmean[3] = {1.2918628e+2, 1.0476238e+2, 93.5939636};
	for(int b=0; b<batch; b++){
		for(int i=0; i<3; i++){
			for(int j=0; j<channelSize; j++, g_cnt++){
				input_copy[g_cnt] = input[g_cnt] - imgmean[i];
			}
		}
	}

	void *input_data;
	cudaMalloc((void **)&input_data, inputSize*sizeof(float));
	cudaMemcpy(input_data, input_copy, inputSize*sizeof(float), cudaMemcpyHostToDevice); 
	delete [] input_copy;
	tensor = makeTensor(vl::TensorShape(224,224,3,batch),input_data,inputSize*sizeof(float),vl::Device::GPU);
	//conv1_1
	tensor = vgg_conv_wrap(ctx, tensor, 0);
	//conv1_2
	tensor = vgg_conv_wrap(ctx, tensor, 1);
	//mpool1
	tensor = vgg_pool_wrap(ctx, tensor, vl::TensorShape(112,112,64,batch));
	//conv2_1
	tensor = vgg_conv_wrap(ctx, tensor, 2);
	//conv2_2
	tensor = vgg_conv_wrap(ctx, tensor, 3);
	//mpool1
	tensor = vgg_pool_wrap(ctx, tensor, vl::TensorShape(56,56,128,batch));

	//conv3_1
	tensor = vgg_conv_wrap(ctx, tensor, 4);
	//conv3_2
	tensor = vgg_conv_wrap(ctx, tensor, 5);
	//conv3_3
	tensor = vgg_conv_wrap(ctx, tensor, 6);
	//mpool1
	tensor = vgg_pool_wrap(ctx, tensor, vl::TensorShape(28,28,256,batch));

	//conv4_1
	tensor = vgg_conv_wrap(ctx, tensor, 7);
	//conv4_2
	tensor = vgg_conv_wrap(ctx, tensor, 8);
	//conv4_3
	tensor = vgg_conv_wrap(ctx, tensor, 9);
	//mpool1
	tensor = vgg_pool_wrap(ctx, tensor, vl::TensorShape(14,14,512,batch));

	//conv5_1
	tensor = vgg_conv_wrap(ctx, tensor, 10);
	//conv5_2
	tensor = vgg_conv_wrap(ctx, tensor, 11);
	//conv5_3
	tensor = vgg_conv_wrap(ctx, tensor, 12);
	//mpool5
	tensor = vgg_pool_wrap(ctx, tensor, vl::TensorShape(7,7,512,batch));

	//fc6
	tensor = vgg_fc_wrap(ctx, tensor, 13, true);
	//fc7
	tensor = vgg_fc_wrap(ctx, tensor, 14, false);
	//fc8
	//tensor = vgg_fc_wrap(ctx, tensor, 15, false);
	float *out = (float*)moveCPU(tensor);
	return out;
}
}
