
#include "vgg_face_network.h"
#include <iostream>
#include <fstream>
#include <windows.h>
#include <thread>
#include <vector>
#include <future>


void copyFromBinary(std::string filename, void *data, size_t size){
	std::ifstream fin;
	fin.open(filename, std::ios::binary | std::ios::in);
	fin.read((char*)data, size);
	fin.close();
}

#define SEL_LAYER 21

void process(float * data, int batch, vgg_face_network * inst, int *time, float * sample_out){
	float *out = new float[1];
	long time_acc = 0;
	int maxiter = 1000;
	for(int i=0; i<maxiter; i++){
		delete [] out;
		DWORD dw1 = GetTickCount();
		inst->process(data,batch);
		out = inst->get_result(SEL_LAYER);
		DWORD dw2 = GetTickCount();
		for (int i=0; i<inst->get_shape(SEL_LAYER).getNumElements(); i++){
			if (out[i] != sample_out[i]){
				std::cout << "Ooops..! " << out[i] << " " << sample_out[i] << " " << sample_out[i] - out[i] << std::endl;
				break;
			}
		}
		time_acc += dw2-dw1;
	}
	delete [] out;
	*time = time_acc/maxiter;
}

float * image;

int main(){
	// initialize the network by using this (path must end with '/')
	vgg_face_network vggface("../data/vgg-face/", "../data/vgg-face/meta.txt", DEVICE_CPU);
	vggface.load_network();
	//vggface.print_meta();
	//return 0 ;
	std::cout<<"VGG LOADED SUCESSFULLY"<<std::endl;

	// size of an image
	int numel_img = 3*224*224;
	
	//batch size
	int batch = 1;

	float *imgdata = new float[numel_img*batch];  // note the size of input

	
	// copyFromBinary is included in vggface.hpp
	copyFromBinary("../data/face.dat",imgdata,numel_img*sizeof(float));  // sample raw image(224x224x3)

	// copy an image multiple times for batch input (for test)
	for(int i=1; i<batch; i++){
		memcpy(imgdata+numel_img*i,imgdata, numel_img*sizeof(float));
	}
	vggface.process(imgdata, batch);
	float * sample_out = vggface.get_result(SEL_LAYER);

	
	float sum = 0;
	for (int i=0; i<vggface.get_shape(SEL_LAYER).getNumElements(); i++){
		//std::cout << sample_out[i] << std::endl;
		sum += sample_out[i]*sample_out[i];
	}
	std::cout << std::endl << sum << std::endl;
	

	for (int i=0; i< 100; i++){
		DWORD dw1 = GetTickCount();
		vggface.process(imgdata, batch);
		DWORD dw2 = GetTickCount();
		float *out = vggface.get_result(SEL_LAYER);
		std::cout << dw2 - dw1 <<std::endl;
		for (int i=0; i<vggface.get_shape(SEL_LAYER).getNumElements(); i++){
			if (out[i] != sample_out[i]){
				std::cout << "Ooops..! " << out[i] << " " << sample_out[i] << " " << sample_out[i] - out[i] << std::endl;
				break;
			}
		}
		delete [] sample_out;
		sample_out = out;
	}
	
	vgg_face_network vgg2(vggface);
	vggface.unload_network();
	//vgg_face_network vgg3(vggface);
	//vgg_face_network vgg4(vggface);
	int time1, time2, time3, time4;
	time1 = time2 = time3 = time4 = 0;
	std::cout << time1 << " " << time2 << " " << time3 << " " << time4;
	//std::thread a(process, imgdata, batch, &vggface, &time1, sample_out);
	std::thread b(process, imgdata, batch, &vgg2, &time2, sample_out);
	//std::thread c(process, imgdata, batch, &vgg3, &time3, sample_out);
	//std::thread d(process, imgdata, batch, &vgg4, &time4, sample_out);
	//std::thread a(process, imgdata, batch, vggface);
	//std::thread b(process, imgdata, batch, vgg2);
	//TEST END
	//a.join();
	b.join();
	//c.join();
	//d.join();
	std::cout << time1 << " " << time2 << " " << time3 << " " << time4;
	delete [] sample_out;
	delete [] imgdata;
	return 0;
}
