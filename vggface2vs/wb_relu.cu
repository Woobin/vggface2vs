/*
* Author: Woobin Im
* Description: RELU forward implementation
*/
#include "wb_relu.hpp"

// CUDA function
template <class T>
void __global__ forward(T * const x, T * y, const int N)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < N){
		if (x[i] < 0)
			y[i] = 0;
		else y[i] = x[i];
	}
}

// Wrap function
void wb_relu_forward_gpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	int numThreadPerBlock = 1024;
	int inputSize = x.getShape().getNumElements();
	int num_blocks= (inputSize + numThreadPerBlock - 1) / numThreadPerBlock;
	forward<float><<<num_blocks, numThreadPerBlock>>>((float *)x.getMemory(),(float *)y.getMemory(), inputSize);
}

// Wrap function
void wb_relu_forward_cpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	int inputSize = x.getShape().getNumElements();
	float * x_data, * y_data;
	x_data = (float*)x.getMemory();
	y_data = (float*)y.getMemory();
	for (int i=0; i<inputSize; i++){
		if (x_data[i] < 0) y_data[i] = 0;
		else y_data[i] = x_data[i];
	}
}