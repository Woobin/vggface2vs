/*
* Author: Woobin Im
* Description: RELU forward implementation
*/
#ifndef WB_RELU_H
#define WB_RELU_H

#include <cuda.h>
#include "bits/data.hpp"

/* USAGE
*  ctx: vl context
*  x: input tensor
*  y: output tensor
*/
void wb_relu_forward_gpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y);
void wb_relu_forward_cpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y);

#endif