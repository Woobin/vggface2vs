// VGG_FACE_NETWORK CLASS IMPLEMENTATION
// Author: Woobin Im

#include "vgg_face_network.h"

#include <iostream>
#include <fstream>
#include <cstring>
#include <cuda_runtime.h>

#include "wb_relu.hpp"
#include "wb_softmax.hpp"
#include "wb_l2_norm.hpp"

#include "bits/nnconv.hpp"
#include "bits/nnpooling.hpp"
#include "bits/nnfullyconnected.hpp"
#include "bits/nnnormalize.hpp"

#include <cassert>
#include <mutex>

#define RETRY 1000

std::mutex thread_weight_del_mtx;

struct layer{
	char name[16];
	char type[16];
	struct layer * bottom;
	char w_path[64];
	char b_path[64];
	vl::TensorShape w_shape;
	vl::TensorShape b_shape;
	void * w_data;
	void * b_data;
	vl::TensorShape in_shape;
	vl::TensorShape out_shape;
	void * in_tensor_data;
	void * out_tensor_data;
};

// vgg convolution
vl::ErrorCode vgg_conv(vl::Context &ctx, vl::Tensor x, vl::Tensor w, vl::Tensor b, vl::Tensor y){
	vl::ErrorCode res =  vl::nnconv_forward(ctx, y, 1, x, 1, w, b, 1, 1, 1, 1, 1, 1, 1, 1);
	return res;
}

// vgg fully connected
vl::ErrorCode vgg_fc(vl::Context &ctx, vl::Tensor x, vl::Tensor w, vl::Tensor b, vl::Tensor y){
	vl::ErrorCode res =  vl::nnfullyconnected_forward(ctx, y, x, w, b) ;
	return res;
	
}

// vgg pooling
vl::ErrorCode vgg_pool(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	return vl::nnpooling_forward(ctx, y, x, vl::vlPoolingMax, 2, 2, 2, 2, 0, 0, 0, 0) ;
}

// vgg relu
void vgg_relu(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	// this function is not part of MatConvNet
	if (x.getDeviceType() == vl::DeviceType::VLDT_GPU)
		wb_relu_forward_gpu(ctx, x, y);
	else
		wb_relu_forward_cpu(ctx, x, y);
}

// vgg softmax
void vgg_softmax(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	// this function is not part of MatConvNet
	if (x.getDeviceType() == vl::DeviceType::VLDT_GPU)
		wb_softmax_forward_gpu(ctx, x, y);
	else
		wb_softmax_forward_cpu(ctx, x, y);
}

vl::ErrorCode embed_l2_norm(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	if (x.getDeviceType() == vl::DeviceType::VLDT_GPU)
		return vl::nnlrn_forward(ctx, y, x, y.getShape().getDepth()*2, 0, 1, 0.5) ;
	else {
		wb_l2_norm_forward_cpu(ctx, x, y);
		return vl::ErrorCode::VLE_Success;
	}
}

vgg_face_network::vgg_face_network(std::string vggface_path, std::string meta_path,
								   VGG_FACE_Device device_type):model_path(vggface_path), device_type(device_type), meta_path(meta_path){
	#ifdef DEBUG_VERBOSE
	std::cout << "vgg_face_network created. net path: " << vggface_path << std::endl;
	#endif
	layers=nullptr;
	io_filled=false;
	is_loaded=false;
	class_clones = nullptr;
	build_model();
}

vgg_face_network::vgg_face_network(const vgg_face_network &init_class){
	#ifdef DEBUG_VERBOSE
	std::cout << "vgg_face_network copied with copy constructor." << std::endl;
	#endif
	model_path = init_class.model_path;
	meta_path = init_class.meta_path;

	// Copy network data
	layer_depth = init_class.layer_depth;
	layers = new layer[layer_depth];
	for (int i=0; i<layer_depth; i++){
		layers[i] = init_class.layers[i];
		layers[i].in_tensor_data = nullptr;
		layers[i].out_tensor_data = nullptr;
		if (init_class.layers[i].bottom == nullptr){
			layers[i].bottom = nullptr;
		}
		else {
			long long diff = (layer*)&(init_class.layers[i]) - init_class.layers[i].bottom;
			layers[i].bottom = (layer*)(&(layers[i])) - diff;
		}
	}
	base_io_tensor_len = init_class.base_io_tensor_len;
	is_loaded = init_class.is_loaded;

	class_clones = init_class.class_clones;


	std::unique_lock<std::mutex> lck (thread_weight_del_mtx,std::defer_lock);
	lck.lock();
	if (class_clones->empty())
		is_loaded = false;
	class_clones->push_back(this);
	lck.unlock();

	context.clear();

	io_data = nullptr;
	io_filled = false;
	io_batch_size = init_class.io_batch_size;
	device_type = init_class.device_type;

	if (init_class.io_filled)
		fill_io_tensors(io_batch_size);
}
vgg_face_network::~vgg_face_network(){
	#ifdef DEBUG_VERBOSE
	std::cout << "~vgg_face_network() called" << std::endl;
	#endif
	unload_network();
	delete [] layers;
}

void vgg_face_network::unload_network(){
	empty_io_tensors();
	if (!is_loaded){
		return;
	}
	#ifdef DEBUG_VERBOSE
	std::cout << "unload_network() called" << std::endl;
	#endif

	std::vector<vgg_face_network*>::iterator it;
	for (it = class_clones->begin(); it != class_clones->end(); it++){
		if (*it == this) break;
	}

	std::unique_lock<std::mutex> lck (thread_weight_del_mtx,std::defer_lock);
	lck.lock();
	class_clones->erase(it);
	bool to_unload_weight = class_clones->empty();
	lck.unlock();

	#ifdef DEBUG_VERBOSE
	if (to_unload_weight){
		std::cout << "delete all weight option selected" << std::endl;
	}
	else {
		std::cout << "remain copy: " << class_clones->size() << std::endl;
	}
	#endif

	class_clones = nullptr;

	for (int i=0; i<layer_depth; i++){
		layers[i].in_tensor_data = layers[i].out_tensor_data = nullptr;
		
		if (to_unload_weight && layers[i].w_data != nullptr){
			if (device_type == vl::DeviceType::VLDT_GPU)
				cudaFree(layers[i].w_data);
			else
				delete [] layers[i].w_data;
		}
		if (to_unload_weight && layers[i].b_data != nullptr){
			if (device_type == vl::DeviceType::VLDT_GPU)
				cudaFree(layers[i].b_data);
			else
				delete [] layers[i].b_data;
		}
		layers[i].w_data = nullptr;
		layers[i].b_data = nullptr;
	}
	is_loaded = false;
}

// Network configuration
void vgg_face_network::build_model(){
	#ifdef DEBUG_VERBOSE
	std::cout << "vgg_face_network building network..." << std::endl;
	#endif
	std::ifstream meta(meta_path);

	if (!meta.is_open()){
		std::cout << "There's no meta file for network..." << std::endl;
		return;
	}

	meta >> layer_depth;
	layers = new layer[layer_depth];
	for (int i=0; i<layer_depth; i++){
		layer temp;
		memset(&temp,0,sizeof(layer));
		char a;
		int h, w, d, s;
		std::string trash;
		meta >> a;
		while (a != '{'){
			meta >> a;
			if (a == meta.eof()){
				meta.close();
				return;
			}
		}
		
		meta >> trash >> layers[i].name;
		meta >> trash >> layers[i].type;
		meta >> trash;
		if (trash == "bottom:"){
			std::string bottom_name;
			meta >> bottom_name >> trash;
			int j = i-1;
			for (; j>=-1; j--){
				assert(j >= -1);
				if (strcmp(layers[j].name, bottom_name.c_str()) == 0){
					layers[i].bottom = &(layers[j]);
					break;
				}
			}
		}
		else {
			if (i == 0) layers[i].bottom = nullptr;
			else layers[i].bottom = &(layers[i-1]);
		}
		if (strcmp(layers[i].type,"conv") == 0 || 
			strcmp(layers[i].type,"fc") == 0 || 
			strcmp(layers[i].type,"fc_norelu") == 0){
			meta >> layers[i].w_path;
			meta >> trash >> layers[i].b_path;
		}
		else {
			meta >> trash;
			strcpy(layers[i].w_path, "");
			strcpy(layers[i].b_path, "");
		}
		
		meta >> trash >> h >> w >> d >> s;
		layers[i].w_shape = vl::TensorShape(h,w,d,s);
		meta >> trash >> h >> w >> d >> s;
		layers[i].b_shape = vl::TensorShape(h,w,d,s);
		meta >> trash >> h >> w >> d >> s;
		layers[i].in_shape = vl::TensorShape(h,w,d,s);
		meta >> trash >> h >> w >> d >> s;
		layers[i].out_shape = vl::TensorShape(h,w,d,s);
		#ifdef DEBUG_VERBOSE
		std::cout << "layer " << i << "(" << layers[i].name << ") meta loaded" << std::endl;
		#endif
	}
	meta.close();
	#ifdef DEBUG_VERBOSE
	print_meta();
	#endif
	verify_model();
}

void vgg_face_network::print_meta(){
	std::cout << layer_depth << std::endl;
	for (int i=0; i<layer_depth; i++){
		std::cout << "layer {" << std::endl;
		std::cout << "name: " << layers[i].name << std::endl;
		std::cout << "type: " << layers[i].type << std::endl;
		if (i != 0)
			std::cout << "bottom: " << (layers[i].bottom)->name << std::endl;
		std::cout << "w_path: " << layers[i].w_path << std::endl;
		std::cout << "b_path: " << layers[i].b_path << std::endl;
		std::cout << "w_shape: " << layers[i].w_shape.getHeight() << " "
								 << layers[i].w_shape.getWidth() << " "
								 << layers[i].w_shape.getDepth() << " "
								 << layers[i].w_shape.getSize() << std::endl;
		std::cout << "b_shape: " << layers[i].b_shape.getHeight() << " "
								 << layers[i].b_shape.getWidth() << " "
								 << layers[i].b_shape.getDepth() << " "
								 << layers[i].b_shape.getSize() << std::endl;
		std::cout << "in_shape: " << layers[i].in_shape.getHeight() << " "
								 << layers[i].in_shape.getWidth() << " "
								 << layers[i].in_shape.getDepth() << " "
								 << "1" << std::endl;
		std::cout << "out_shape: " << layers[i].out_shape.getHeight() << " "
								 << layers[i].out_shape.getWidth() << " "
								 << layers[i].out_shape.getDepth() << " "
								 << "1" << std::endl;
		std::cout << "}" << std::endl;
	}
}

int vgg_face_network::verify_model(){
	#ifdef DEBUG_VERBOSE
		std::cout << "Verify model.." << std::endl;
	#endif
	if (layers == nullptr){
		std::cout << "Network has not been built" << std::endl;
		return 0;
	}
	#ifdef DEBUG_VERBOSE
		std::cout << "Verified" << std::endl;
	#endif
	return 1;
}

void vgg_face_network::load_network(){
	#ifdef DEBUG_VERBOSE
	std::cout << "loading network.." << std::endl;
	#endif
	unload_network();
	base_io_tensor_len = 0;

	class_clones = new std::vector<vgg_face_network*>;
	class_clones->push_back(this);

	layer *cur_layer;
	base_io_tensor_len += (layers[0].in_shape).getNumElements();
	for (int i=0; i<layer_depth; i++){
		cur_layer = &(layers[i]);
		(cur_layer->in_shape).setSize(1);
		(cur_layer->out_shape).setSize(1);
		if (strcmp(cur_layer->type, "conv") == 0 || 
			strcmp(cur_layer->type, "fc") == 0 || 
			strcmp(cur_layer->type, "fc_norelu") == 0){
			//std::cout << cur_layer->type;
			size_t w_size = (cur_layer->w_shape).getNumElements() * sizeof(float);
			size_t b_size = (cur_layer->b_shape).getNumElements() * sizeof(float);

			allocateMemoryFromBinary(model_path + cur_layer->w_path, cur_layer->w_data, w_size);
			allocateMemoryFromBinary(model_path + cur_layer->b_path, cur_layer->b_data, b_size);
		}
		else {
			cur_layer->w_data = nullptr;
			cur_layer->b_data = nullptr;
			strcpy(cur_layer->w_path,"");
			strcpy(cur_layer->b_path,"");
		}
		base_io_tensor_len += (cur_layer->out_shape).getNumElements();
	}
	is_loaded = true;
}

// copy values from binary file to GPU memory or CPU memory
void vgg_face_network::allocateMemoryFromBinary(std::string filename, void * &data, size_t size){
	void *temp = new char[size];
	copyFromBinary(filename, temp, size);

	if (device_type == vl::DeviceType::VLDT_GPU){
		cudaMalloc((void **)&data, size);
		cudaMemcpy(data, temp, size, cudaMemcpyHostToDevice);
	}
	else {
		data = new char[size];
		memcpy(data, temp, size);
	}
	delete [] temp;
}

// copy values in binary file to memory.
void vgg_face_network::copyFromBinary(std::string filename, void *data, size_t size){
	std::ifstream fin;
	fin.open(filename, std::ios::binary | std::ios::in);
	fin.read((char*)data, size);
	fin.close();
}

// move values in gpu tensor to cpu memory
void* vgg_face_network::moveCPU(vl::Tensor tensor){
	int size = tensor.getShape().getNumElements()*sizeof(tensor.getDataType());
	if(tensor.getDeviceType() == vl::DeviceType::VLDT_GPU){
		void* ongpu = tensor.getMemory();
		void* oncpu = new char[size];
		cudaMemcpy(oncpu, ongpu, size, cudaMemcpyDeviceToHost);
		return oncpu;
	}
	else {
		void* on_io = tensor.getMemory();
		void* on_dest = new char[size];
		memcpy(on_dest, on_io, size);
		return on_dest;
	}
}
void vgg_face_network::empty_io_tensors(){
	#ifdef DEBUG_VERBOSE
	std::cout << "empty_io_tensors() called" << std::endl;
	#endif
	if (!io_filled){
		return;
	}
	if (device_type == vl::DeviceType::VLDT_GPU)
		cudaFree(io_data);
	else
		delete [] io_data;
	io_data = nullptr;
	io_filled = false;
}

void vgg_face_network::fill_io_tensors(int batch_size){
	#ifdef DEBUG_VERBOSE
	std::cout << "fill_io_tensors(int batch_size) called" << std::endl;
	#endif
	if (!is_loaded){
		load_network();
	}
	if (io_filled){
		empty_io_tensors();
	}

	if (device_type == vl::DeviceType::VLDT_GPU){
		if (cudaMalloc((void **)&(io_data), 
			base_io_tensor_len*batch_size*sizeof(float)) != cudaSuccess){
			std::cout << "Cannot allocate cuda memory for IO space." << std::endl;
		}
	}
	else {
		io_data = (float*)new char[base_io_tensor_len*batch_size*sizeof(float)];
	}

	int start_point_offset = 0;
	layer *cur_layer;
	for (int i=0; i<layer_depth; i++){
		cur_layer = &(layers[i]);

		(cur_layer->in_shape).setSize(batch_size);
		(cur_layer->out_shape).setSize(batch_size);

		if (cur_layer->bottom == nullptr){
			cur_layer->in_tensor_data = (io_data + start_point_offset);
			start_point_offset += (cur_layer->in_shape).getNumElements();
		}
		else {
			cur_layer->in_tensor_data = (cur_layer->bottom)->out_tensor_data;
		}
		cur_layer->out_tensor_data = (io_data + start_point_offset);
		start_point_offset += (cur_layer->out_shape).getNumElements();
	}

	io_filled = true;
	io_batch_size = batch_size;

	return;
}

TensorShape vgg_face_network::get_shape(int layer){
	#ifdef DEBUG_VERBOSE_HIGH
	std::cout << "get_shape(int layer) called" << std::endl;
	#endif
	assert(layer < layer_depth);
	return layers[layer].out_shape;
}

TensorShape vgg_face_network::get_shape(std::string layername){
	#ifdef DEBUG_VERBOSE_HIGH
	std::cout << "get_shape(std::string layername) called" << std::endl;
	#endif
	int layer = -1;
	for (int i=layer_depth-1; i>=0; i--){
		if (strcmp(layers[i].name, layername.c_str()) == 0){
			layer = i;
			break;
		}
	}
	assert (layer >= 0);
	return get_shape(layer);
}

float * vgg_face_network::get_result(int layer){
	#ifdef DEBUG_VERBOSE_HIGH
	std::cout << "get_result(int layer) called" << std::endl;
	#endif
	assert(layer < layer_depth);
	vl::Tensor tensor(layers[layer].out_shape, vl::VLDT_Float, 
		device_type, layers[layer].out_tensor_data, 
		layers[layer].out_shape.getNumElements()*sizeof(float));
	float *out = (float*)moveCPU(tensor);
	return out;
}

float * vgg_face_network::get_result(std::string layername){
	#ifdef DEBUG_VERBOSE_HIGH
	std::cout << "get_result(std::string layername) called" << std::endl;
	#endif
	int layer = -1;
	for (int i=layer_depth-1; i>=0; i--){
		if (strcmp(layers[i].name, layername.c_str()) == 0){
			layer = i;
			break;
		}
	}
	assert (layer >= 0);

	float *out = get_result(layer);
	return out;
}

void vgg_face_network::process(float *input, int batch_size){
	#ifdef DEBUG_VERBOSE
	std::cout << "vgg_face processing image." << std::endl;
	#endif
	if (!io_filled || io_batch_size != batch_size){
		fill_io_tensors(batch_size);
	}

	int img_size = 224*224*3;
	int input_size = img_size*batch_size;
	
	//Input
	float * input_copy = new float[input_size];
	
	int g_cnt = 0;
	int channelSize = img_size/3;
	float imgmean[3] = {1.2918628e+2, 1.0476238e+2, 93.5939636};
	for(int b=0; b<batch_size; b++){
		for(int i=0; i<3; i++){
			for(int j=0; j<channelSize; j++, g_cnt++){
				input_copy[g_cnt] = input[g_cnt] - imgmean[i];
			}
		}
	}
	if (device_type == vl::DeviceType::VLDT_GPU){
		if (cudaMemcpy(layers[0].in_tensor_data, input_copy, 
			input_size*sizeof(float), cudaMemcpyHostToDevice) != cudaSuccess){
			std::cout << "Cannot copy input to cuda memory" << std::endl;
		}
	}
	else {
		memcpy(layers[0].in_tensor_data, input_copy, input_size*sizeof(float));
	}
	delete [] input_copy;

	vl::Tensor tensor;
	
	layer *cur_layer;
	for (int i=0; i<layer_depth; i++){
		cur_layer = &(layers[i]);
		if (strcmp(cur_layer->type, "conv") == 0){
			tensor = vgg_conv_wrap(context, i);
		}
		else if (strcmp(cur_layer->type, "pool") == 0){
			tensor = vgg_pool_wrap(context, i);
		}
		else if (strcmp(cur_layer->type, "fc") == 0){
			tensor = vgg_fc_wrap(context, i, true);
		}
		else if (strcmp(cur_layer->type, "fc_norelu") == 0){
			tensor = vgg_fc_wrap(context, i, false);
		}
		else if (strcmp(cur_layer->type, "softmax") == 0){
			tensor = vgg_softmax_wrap(context, i);
		}
		else if (strcmp(cur_layer->type, "normalize") == 0){
			tensor = embed_l2_norm_wrap(context, i);
		}
		else {
			std::cout << "Unknown layer: " << cur_layer->type << std::endl;
			exit (-1);
		}
	}
	context.clear();
}

// vgg convolution wrapping for the convinient use.
vl::Tensor vgg_face_network::vgg_conv_wrap(vl::Context &ctx, int layer_num){
	#ifdef DEBUG_VERBOSE_LAYER
	std::cout << "vgg_conv_wrap called" << std::endl;
	#endif
	layer * cur_layer = &(layers[layer_num]);

	size_t i_size = (cur_layer->in_shape).getNumElements()*sizeof(float);
	size_t o_size = (cur_layer->out_shape).getNumElements()*sizeof(float);
	size_t w_size = (cur_layer->w_shape).getNumElements()*sizeof(float);
	size_t b_size = (cur_layer->b_shape).getNumElements()*sizeof(float);

	vl::Tensor iTensor((cur_layer->in_shape),vl::VLDT_Float, device_type, cur_layer->in_tensor_data, i_size);
	vl::Tensor oTensor((cur_layer->out_shape),vl::VLDT_Float, device_type, cur_layer->out_tensor_data, o_size);
	vl::Tensor wTensor((cur_layer->w_shape),vl::VLDT_Float, device_type, cur_layer->w_data, w_size);
	vl::Tensor bTensor((cur_layer->b_shape),vl::VLDT_Float, device_type, cur_layer->b_data, b_size);

	
	if (device_type == vl::DeviceType::VLDT_GPU){
		cudaMemset(cur_layer->out_tensor_data,0,o_size);
		cudaDeviceSynchronize();
	}
	else {
		memset(cur_layer->out_tensor_data,0,o_size);
	}

	int cnt = 0;
	while (vgg_conv(ctx, iTensor, wTensor, bTensor, oTensor) != 0){
		if (++cnt > RETRY)
			exit(-1);
	}

	vgg_relu(ctx, oTensor, oTensor);
	return oTensor;
}

// vgg fully connected wrapping for the convinient use.
vl::Tensor vgg_face_network::vgg_fc_wrap(vl::Context &ctx, int layer_num, bool relu){
	#ifdef DEBUG_VERBOSE_LAYER
	std::cout << "vgg_fc_wrap called" << std::endl;
	#endif
	layer * cur_layer = &(layers[layer_num]);

	size_t i_size = (cur_layer->in_shape).getNumElements()*sizeof(float);
	size_t o_size = (cur_layer->out_shape).getNumElements()*sizeof(float);
	size_t w_size = (cur_layer->w_shape).getNumElements()*sizeof(float);
	size_t b_size = (cur_layer->b_shape).getNumElements()*sizeof(float);

	vl::Tensor iTensor((cur_layer->in_shape),vl::VLDT_Float, device_type, cur_layer->in_tensor_data, i_size);
	vl::Tensor oTensor((cur_layer->out_shape),vl::VLDT_Float, device_type, cur_layer->out_tensor_data, o_size);
	vl::Tensor wTensor((cur_layer->w_shape),vl::VLDT_Float, device_type, cur_layer->w_data, w_size);
	vl::Tensor bTensor((cur_layer->b_shape),vl::VLDT_Float, device_type, cur_layer->b_data, b_size);

	if (device_type == vl::DeviceType::VLDT_GPU){
		cudaMemset(cur_layer->out_tensor_data,0,o_size);
		cudaDeviceSynchronize();
	}
	else {
		memset(cur_layer->out_tensor_data,0,o_size);
	}

	int cnt = 0;
	while (vgg_fc(ctx, iTensor, wTensor, bTensor, oTensor) != 0){
		if (++cnt > RETRY)
			exit(-1);
	}
	if (relu)
		vgg_relu(ctx, oTensor, oTensor);
	return oTensor;
}

// vgg pooling wrapping for the convinient use.
vl::Tensor vgg_face_network::vgg_pool_wrap(vl::Context &ctx, int layer_num){
	#ifdef DEBUG_VERBOSE_LAYER
	std::cout << "vgg_pool_wrap called" << std::endl;
	#endif
	layer * cur_layer = &(layers[layer_num]);

	size_t i_size = (cur_layer->in_shape).getNumElements()*sizeof(float);
	size_t o_size = (cur_layer->out_shape).getNumElements()*sizeof(float);
	vl::Tensor iTensor((cur_layer->in_shape),vl::VLDT_Float, device_type, cur_layer->in_tensor_data, i_size);
	vl::Tensor oTensor((cur_layer->out_shape),vl::VLDT_Float, device_type, cur_layer->out_tensor_data, o_size);
	
	int cnt = 0;
	while (vgg_pool(ctx, iTensor, oTensor) != 0){
		if (++cnt > RETRY)
			exit(-1);
	}
	return oTensor;
}

// vgg softmax wrapping for the convinient use.
vl::Tensor vgg_face_network::vgg_softmax_wrap(vl::Context &ctx, int layer_num){
	#ifdef DEBUG_VERBOSE_LAYER
	std::cout << "vgg_softmax_wrap called" << std::endl;
	#endif
	layer * cur_layer = &(layers[layer_num]);

	size_t i_size = (cur_layer->in_shape).getNumElements()*sizeof(float);
	size_t o_size = (cur_layer->out_shape).getNumElements()*sizeof(float);
	vl::Tensor iTensor((cur_layer->in_shape),vl::VLDT_Float, device_type, cur_layer->in_tensor_data, i_size);
	vl::Tensor oTensor((cur_layer->out_shape),vl::VLDT_Float, device_type, cur_layer->out_tensor_data, o_size);
	
	vgg_softmax(ctx, iTensor, oTensor);
	return oTensor;
}

vl::Tensor vgg_face_network::embed_l2_norm_wrap(vl::Context &ctx, int layer_num){
	#ifdef DEBUG_VERBOSE_LAYER
	std::cout << "embed_l2_norm_wrap called" << std::endl;
	#endif
	layer * cur_layer = &(layers[layer_num]);

	size_t i_size = (cur_layer->in_shape).getNumElements()*sizeof(float);
	size_t o_size = (cur_layer->out_shape).getNumElements()*sizeof(float);
	vl::Tensor iTensor((cur_layer->in_shape),vl::VLDT_Float, device_type, cur_layer->in_tensor_data, i_size);
	vl::Tensor oTensor((cur_layer->out_shape),vl::VLDT_Float, device_type, cur_layer->out_tensor_data, o_size);
	
	embed_l2_norm(ctx, iTensor, oTensor);
	return oTensor;
}