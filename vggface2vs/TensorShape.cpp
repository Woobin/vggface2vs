#include "vgg_face_network.h"

TensorShape::TensorShape (const vl::TensorShape& x){
	memcpy(this->dimensions, x.getDimensions(), x.getNumDimensions()*sizeof(size_t));
	this->numDimensions = x.getNumDimensions();
}

TensorShape& TensorShape::operator= (const vl::TensorShape& x){
	memcpy(this->dimensions, x.getDimensions(), x.getNumDimensions()*sizeof(size_t));
	this->numDimensions = x.getNumDimensions();
	return *this;
}

size_t TensorShape::getDimension(size_t num) const {
	return this->numDimensions;
}

size_t const * TensorShape::getDimensions() const {
	return this->dimensions;
}


size_t TensorShape::getNumDimensions() const {
	return this->numDimensions;
}

size_t TensorShape::getHeight() const {
	return this->dimensions[0];
}

size_t TensorShape::getWidth() const {
	return this->dimensions[1];
}

size_t TensorShape::getDepth() const {
	return this->dimensions[2];
}

size_t TensorShape::getSize() const {
	return this->dimensions[3];
}

size_t TensorShape::getNumElements() const {
	if (numDimensions == 0) {
		return 0;
	}
	size_t n = 1;
	for (unsigned k = 0; k<numDimensions; ++k){
		n *= dimensions[k]; 
	}
	return n ;
}