/*
* Author: Woobin Im
* Description: VGG-face feature extractor.
*/
#ifndef VGGFACE_H
#define VGGFACE_H
#include "bits/nnconv.hpp"
#include "bits/nnpooling.hpp"
#include "bits/nnfullyconnected.hpp"
#include "bits/data.hpp"
#include <iostream>
#include <fstream>
#include <vector>

namespace vggface {
	// use it to initialize network. Don't use it more than once.
	void initializeNet(std::string netpath);

	// feature extractor (input size: 224x224x3xbatch)
	float * vgg_process(float *input, int batch);

	// read binary data into memory
	void copyFromBinary(std::string filename, void *data, size_t size);
}
#endif