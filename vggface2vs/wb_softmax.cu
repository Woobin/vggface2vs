/*
* Author: Woobin Im
* Description: RELU forward implementation
*/
#include "wb_softmax.hpp"
#include <cuda_runtime.h>
#include <iostream>
#include <math.h>

// CUDA function
template <class T>
void __global__ reduce_max(T * const x, T * max, const int C, const int B)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < B){
		int ptr = i*C;
		int end_ptr = (i+1)*C;
		T * max_ptr = &(max[i]);
		*max_ptr = x[ptr];
		while (++ptr < end_ptr){
			if (x[ptr] > *max_ptr)
				*max_ptr = x[ptr];
		}
	}
}

template <class T>
void __global__ elem_minus_exp(T * const x, T * y, const T * max, const int C, const int B)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < C*B){
		y[i] = expf(x[i] - max[i/C]);
	}
}

template <class T>
void __global__ reduce_sum(T * const x, T * y, const int C, const int B)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < B){
		int ptr = i*C;
		int end_ptr = (i+1)*C;
		y[i] = 0;
		while (ptr < end_ptr){
			y[i] += x[ptr];
			ptr++;
		}
	}
}

template <class T>
void __global__ reduce_divide(T * const x, T * y, const T * expsum, const int C, const int B)
{
	int i = blockDim.x * blockIdx.x + threadIdx.x;
    if (i < C*B){
		y[i] = x[i]/expsum[i/C];
	}
}

// Wrap function
void wb_softmax_forward_gpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	int numThreadPerBlock = 4;
	int inputSize = x.getShape().getNumElements();
	int C = inputSize / x.getSize();
	int B = inputSize / x.getDepth();

	int num_full_blocks= (inputSize + numThreadPerBlock - 1) / numThreadPerBlock;
	int num_batch_blocks= (B + numThreadPerBlock - 1) / numThreadPerBlock;
	
	float * space;
	cudaMalloc((void **)&space, sizeof(float)*B);
	reduce_max<float><<<num_batch_blocks, numThreadPerBlock>>>((float *)x.getMemory(),(float *)space, C, B);
	elem_minus_exp<float><<<num_full_blocks, numThreadPerBlock>>>((float *)x.getMemory(),(float *)x.getMemory(), (float *)space, C, B);
	reduce_sum<float><<<num_batch_blocks, numThreadPerBlock>>>((float *)x.getMemory(),(float *)space, C, B);
	reduce_divide<float><<<num_full_blocks, numThreadPerBlock>>>((float *)x.getMemory(),(float *)y.getMemory(),(float *)space, C, B);
	cudaFree(space);
}

void wb_softmax_forward_cpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	int inputSize = x.getShape().getNumElements();
	int C = inputSize / x.getSize();   // depth (dimension) of vector
	int B = inputSize / x.getDepth();  // batch size

	float *x_data = (float*)x.getMemory();
	float *y_data = (float*)y.getMemory();

	float *max = new float[B];

	// max value calculation
	int idx, i, j;
	for (i=0; i<B; i++){
		max[i] = -1e+6;
		for (j=0; j<C; j++){
			idx = C*i + j;
			if (x_data[idx] > max[i]){
				max[i] = x_data[idx]; 
			}
		}
	}

	// substract max value and take exp.
	for (i=0; i<B; i++){
		for (j=0; j<C; j++){
			idx = C*i + j;
			y_data[idx] = exp(x_data[idx] - max[i]);
		}
	}

	// for reusing the space
	float *sum = max;

	// reduce sum
	for (i=0; i<B; i++){
		sum[i] = 0;
		for (j=0; j<C; j++){
			idx = C*i + j;
			sum[i] += y_data[idx];
		}
	}

	// reduce divide
	for (i=0; i<B; i++){
		for (j=0; j<C; j++){
			idx = C*i + j;
			y_data[idx] = y_data[idx] / sum[i];
		}
	}

	delete [] sum;
}