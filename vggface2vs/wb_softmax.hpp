/*
* Author: Woobin Im
* Description: Softmax forward implementation
*/
#ifndef WB_SOFTMAX_H
#define WB_SOFTMAX_H

#include <cuda.h>
#include "bits/data.hpp"

/* USAGE
*  ctx: vl context
*  x: input tensor
*  y: output tensor
*/
void wb_softmax_forward_gpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y);
void wb_softmax_forward_cpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y);

#endif