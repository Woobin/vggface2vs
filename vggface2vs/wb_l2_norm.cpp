#include "wb_l2_norm.hpp"
void wb_l2_norm_forward_cpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y){
	int inputSize = x.getShape().getNumElements();
	int C = inputSize / x.getSize();   // depth (dimension) of vector
	int B = inputSize / x.getDepth();  // batch size

	float *x_data = (float*)x.getMemory();
	float *y_data = (float*)y.getMemory();
	

	float *sum = new float[B];
	int i, j, idx;
	for (i=0; i<B; i++){
		sum[i] = 0;
		for (j=0; j<C; j++){
			idx = C*i + j;
			sum[i] += x_data[idx] * x_data[idx];
		}
	}

	float temp_sum;
	for (i=0; i<B; i++){
		temp_sum = sqrt(sum[i]);
		for (j=0; j<C; j++){
			idx = C*i + j;
			y_data[idx] = x_data[idx] / temp_sum;
		}
	}
	delete [] sum;
}