/*
* Author: Woobin Im
* Description: L2_Normalize forward implementation
*/
#ifndef WB_L2_NORM_H
#define WB_L2_NORM_H

#include "bits/data.hpp"

/* USAGE
*  ctx: vl context
*  x: input tensor
*  y: output tensor
*/
void wb_l2_norm_forward_cpu(vl::Context &ctx, vl::Tensor x, vl::Tensor y);

#endif